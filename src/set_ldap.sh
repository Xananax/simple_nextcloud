#!/bin/sh

NXTC_PATH="/var/www/html"
TEST_FILE="$NXT_PATH/ldap_enabled"
SET_APP="$NXTC_PATH/occ config:app:set user_ldap"

run_as() {
    if [ "$(id -u)" = 0 ]; then
        su -p www-data -s /bin/sh -c "$1"
    else
        sh -c "$1"
    fi
}

#if expr "$1" : "apache" 1>/dev/null || [ "$1" = "php-fpm" ] || [ "${NEXTCLOUD_UPDATE:-0}" -eq 1 ]; then
#    if [ ! -f "$TEST_FILE" ]; then
      run_as "php $NXTC_PATH/occ app:enable user_ldap"
      run_as "php $SET_APP ldap_base --value \"dc=yunohost,dc=org\""
      run_as "php $SET_APP ldap_base_groups --value \"ou=groups,dc=yunohost,dc=org\""
      run_as "php $SET_APP ldap_base_users --value \"ou=users,dc=yunohost,dc=org\""
      run_as "php $SET_APP ldap_cache_ttl --value \"600\""
      run_as "php $SET_APP ldap_configuration_active --value \"1\""
      run_as "php $SET_APP ldap_display_name --value \"displayname\""
      run_as "php $SET_APP ldap_email_attr --value \"mail\""
      run_as "php $SET_APP ldap_expert_username_attr --value \"uid\""
      run_as "php $SET_APP ldap_group_display_name --value \"cn\""
      run_as "php $SET_APP ldap_group_filter --value \"objectClass=posixGroup\""
      run_as "php $SET_APP ldap_group_filter_mode --value \"0\""
      run_as "php $SET_APP ldap_groupfilter_objectclass --value \"posixGroup\""
      run_as "php $SET_APP ldap_host --value \"localhost\""
      run_as "php $SET_APP ldap_login_filter --value \"(&(|(objectclass=posixAccount))(uid=%uid))\""
      run_as "php $SET_APP ldap_login_filter_mode --value \"0\""
      run_as "php $SET_APP ldap_port --value \"389\""
      run_as "php $SET_APP ldap_quota_attr --value \"userquota\""
      run_as "php $SET_APP ldap_tls --value \"0\""
      run_as "php $SET_APP user_ldap_display_name --value \"cn\""
      run_as "php $SET_APP user_ldap_filter_mode --value \"0\""
      run_as "php $SET_APP user_ldapfilter_objectclass --value \"posixAccount\""
      run_as "php $SET_APP user_ldaplist_filter --value \"objectclass=posixAccount\""
      touch $TEST_FILE
#    fi
#fi;