# Simple NextCloud

A simple, insecure Nextcloud setup, intended to be used in conjunction with a reverse proxy and an LDAP server.

- clone this repo somewhere
- fill `nextcloud.env` and `db.env`
- fill the mysql password in `docker-compose.yml` (`MYSQL_ROOT_PASSWORD`)
- create a `./nextcloud` directory at the root of your server, and give access to the `www-data` user.
- because of [this bug](https://github.com/nextcloud/docker/issues/593), you need to run the db before, with `sudo docker-compose -f docker-compose.yml up db`. Once it's done (when it says `[Entrypoint]: Temporary server started.`), kill it with ctrl-c.
- run `sudo docker-compose up` (optionally with `-d` to run as a deamon)


Note: If you get a problem running docker-compose, follow [this SO solution](https://stackoverflow.com/questions/33600154/docker-not-starting-could-not-delete-the-default-bridge-network-network-bridg/33604859#33604859)

Run the following to enable LDAP, while the docker is running:

```sh
sudo docker-compose exec app /set_ldap.sh
```

If you want to run [`occ`](https://docs.nextcloud.com/server/15/admin_manual/configuration_server/occ_command.html), this is the way to do it

```sh
sudo docker-compose exec --user www-data app php occ
```

### Data Location

By default, it lives in a `nextcloud` volume. If you dislike it, change the value of `nextcloud` in `docker-compose.yml`. It's used in 3 locations, on lines `23`, `42`, and `51`.

If yoou don't use a Docker volume, then remove the volume at the bottom:

```yml
volumes:
  db:
  nextcloud: #remove this 
```

### YUNOHOST

run

```sh
DOMAIN=<YOUR_DOMAIN>; sudo yunohost app install redirect --label nextcloud --args "domain=$DOMAIN&path=/&redirect_path=http://127.0.0.1:8080&redirect_type=public_proxy"
```

Replace `<YOUR_DOMAIN>` by the domain you intend to run nextcloud on  


## Uninstalling

If you're done testing and want to clean everything, run

```sh
sudo docker-compose down -v && sudo docker-compose stop && sudo docker-compose rm

## caution, `docker prune` cleans all your dockers, not just this one. I'm assuming you're not running any other docker)
sudo docker system prune --volumes -a

# if you used a local directory ./nextcloud, clean it too
sudo rm -rf ./nextcloud
```

Remove the yunohost app.
